///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Kayla_Valera <kvalera@hawaii.edu>
// @date   TODO_09_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>



int main(int argc, char* argv[]) {
	
	time_t now; //present time
	time(&now);


	int tm_year = 0; //print time
	int tm_day = 0;
	int tm_hour = 0;
	int tm_min = 0;
	int tm_secs = 0;

	int yr_in_secs = 31536000; //converting time in terms of seconds
	int day_in_secs = 86400;
	int hour_in_secs = 3600;
	int min_in_secs = 60;
	int count;

struct tm start = {
	.tm_year =120,
	.tm_mon = 8,
	.tm_mday = 22,
	.tm_hour = 10,
	.tm_min = 40,
	.tm_sec = 59
	};

mktime(&start); //make string value from start

printf("The Reference Time is : %s", asctime (&start));

double total_seconds = difftime(now, mktime (&start)); 

printf("%.f seconds have passed since Reference Date.\n", total_seconds);

	while(total_seconds >=yr_in_secs){
		total_seconds = total_seconds - yr_in_secs;
		tm_year = tm_year + 1;
	}

	while(total_seconds >= day_in_secs){
		total_seconds = total_seconds - day_in_secs;
		tm_day = tm_day + 1;
	}
	while(total_seconds >= hour_in_secs){
		total_seconds = total_seconds - hour_in_secs;
		tm_hour = tm_hour +1;
	}
	while(total_seconds >= min_in_secs){
		total_seconds = total_seconds - min_in_secs;
		tm_min = tm_min +1;
	}

	tm_secs = total_seconds;

for(count = 0; count <=60; count++) {

	sleep(1); // 1 second delay
		
printf("Years: %d Days: %d Minutes: %d Seconds: %d\n", tm_year, tm_day, tm_hour, tm_min, tm_secs);

	tm_secs = tm_secs +1;
	
	if(tm_secs == 60){
		tm_min = tm_min +1;
		tm_secs = 0;
	}

	if(tm_min ==60){
		tm_hour = tm_hour+1;
		tm_min = 0;
	}

	if(tm_hour == 24){
		tm_day = tm_day +1;
		tm_hour = 0;
	}

	if(tm_day == 365){
		tm_year = tm_year +1;
		tm_day = 0;
	}
}

   return 0;
}
